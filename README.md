## Nice to meet you! <img src="https://gitea.com/PhNix/.profile/raw/branch/main/assets/wave.gif" width="30">

### About me

I am PhNix(as Phoenix + NixOS), but my name is **Arthur Lokhov**. At the moment I am studying at MIREA. And I get two higher educations. General - **Software Engineering**, and Specialized - **DevOps engineer**.

I am a NeoVim enthusiast, I am NixOS entusiast. And as you could understand, I am a Devops engineer and a Golang developer (who else will put a dancing gopher).

At the moment I have next purposes:

- Become the Gitea active contributor(I hate GitHub, it's proprietary trash, and GitLab too stripped down on the free version).
- Make beatiful NixOS rice(yeah, it's important)
- Become the NeoVim-guru(More keymaps, for God of keymaps).

If you want to contact me.

[![Gmail](https://img.shields.io/badge/ArthurLokhov@bk.ru-44bbff?style=for-the-badge&logo=gmail&logoColor=white)](mailto:arthurlokhov@bk.ru)
[![Telegram](https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/ArtLkv_DV)
[![Reddit](https://img.shields.io/badge/Reddit-FF4500?style=for-the-badge&logo=reddit&logoColor=white)](https://www.reddit.com/user/Ok-Definition-8941)
[![Discord](https://img.shields.io/badge/Discord-%235865F2.svg?style=for-the-badge&logo=discord&logoColor=white)](https://discordapp.com/users/arthurlokhov)

### My developer setup

![Neovim](https://img.shields.io/badge/NeoVim-%2357A143.svg?&style=for-the-badge&logo=neovim&logoColor=white)
![Nix](https://img.shields.io/badge/NIX-5277C3.svg?style=for-the-badge&logo=NixOS&logoColor=white)
![Firefox](https://img.shields.io/badge/Firefox-FF7139?style=for-the-badge&logo=Firefox-Browser&logoColor=white)

### My projects

1. [NvOps](https://gitea.com/PhNix/NvOps) - the Neovim configuration for DevOps.
2. [NixConf](https://gitea.com/PhNix/NixConf) - my NixOS configuration(for now private).
